package edu.gatech.cs2340.scala.threads

object ThreadBasics {

  class ThreadClass(name: String) extends Thread(name) {

    // The run method contains the code that runs when the thread is executed
    override def run(): Unit = {
      println(s"${this.getName} is running")
    }
  }

  def main(args: Array[String]): Unit = {
    println(s"Main thread: ${Thread.currentThread.getName}")

    // After thread t is created, it is in the new state
    val t = new ThreadClass("Guffman")

    // After t is started, it is in the runnable state
    t.start()

    // Once a thread is in the runnable state,
    // it's up to the OS when it actually runs

    // This is the wrong way to wait for a thread to finish because it doesn't
    // put the main thread in a waiting state.
    // Run this program several times to see the waiting message printed
    // a variable number of times
    var i = 1
    while (t.isAlive()) {
      println(s"Waiting for ${t.getName} $i ")
      i += 1
    }

    // t.join() means we wait for t to finish which puts the main thread in
    // the waiting state and signals to the OS that it can suspend the main
    // thread and assign the main thread's processor core to another thread.

    // Comment out the polling loop above and uncommend line below
    // to see the correct waiting behavior
    //t.join()

    // At this point, t is in the terminated state and the main thread is
    // again in the runnable state.
    println(s"${t.getName} joined")


    // Can also create anonymous threads
    val anonThread = new Thread("Anonymous Thread") {
      override def run(): Unit = {
        println(s"${this.getName} is running.")
      }
    }
    anonThread.start()
    anonThread.join()
    println(s"${anonThread.getName} joined")

    // And there's a Java interface called Runnable
    val runnableTask = new Runnable {
      def run(): Unit = {
        // No this because we're impolementing an interface
        println(s"${Thread.currentThread.getName} is running.")
      }
    }
    // To start a thread with a runnable, pass an instance of a class
    // implementing Runnable to the Thread constructor
    val task = new Thread(runnableTask, "Runnable Task")
    task.start()
    task.join()
    println(s"${task.getName} joined")
  }
}
