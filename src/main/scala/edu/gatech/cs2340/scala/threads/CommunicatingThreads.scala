package edu.gatech.cs2340.scala.threads

object CommunicatingThreads {

  var idCount = 0

  // Because this
  def racingGetId() = {
    val newId = idCount + 1
    idCount = newId
    newId
  }

  def synchronizedGetId() = this.synchronized {
    val newId = idCount + 1
    idCount = newId
    newId
  }

  def printFreshIds(n: Int, idGenerator: () => Int): Unit = {
    val ids = for (i <- 0 to n) yield idGenerator()
    println(s"In thread ${Thread.currentThread.getName}, ids: $ids")
  }

  def main(args: Array[String]): Unit = {
    println("Racing threads:")
    val t = new Thread("Racing thread") {
      override def run(): Unit = {
        printFreshIds(5, racingGetId)
      }
    }
    t.start()
    printFreshIds(5, racingGetId)
    t.join()

    println("Synchronized threads:")
    val t2 = new Thread("Synchronized thread") {
      override def run(): Unit = {
        printFreshIds(5, synchronizedGetId)
      }
    }
    t2.start()
    printFreshIds(5, synchronizedGetId)
    t2.join()


  }
}
